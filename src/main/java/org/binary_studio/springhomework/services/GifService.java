package org.binary_studio.springhomework.services;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.binary_studio.springhomework.utils.RequestSender;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.nio.file.*;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@PropertySource("classpath:dev.properties")
public class GifService {

    private final RequestSender requestSender;
    private Map<String, Multimap<String, String>> cache;

    @Value("${folder.cache}")
    private String cacheFolder;

    @Value("${folder.users}")
    private String usersFolder;

    @Autowired
    public GifService(RequestSender requestSender) {
        this.requestSender = requestSender;
    }

    @PostConstruct
    private void initializeCache() {
        cache = new HashMap<>();
    }

    private String copyGifFromCacheToUser(String id, String query) throws IOException {
        List<Path> gifs = Files.list(Paths.get(cacheFolder + query)).collect(Collectors.toList());
        Path gif = gifs.get(new Random().nextInt(gifs.size()));
        String gifName = gif.getFileName().toString();

        if (!Files.exists(Paths.get(usersFolder + id + "/" + query))) {
            new File(usersFolder + id + "/" + query).mkdirs();
        }
        Files.copy(gif, Paths.get(usersFolder + id + "/" + query + "/" + gifName), StandardCopyOption.REPLACE_EXISTING);

        return gifName;
    }

    private String getGifFromServer(String query) throws IOException {
        JSONArray data = requestSender.send(query).getJSONArray("data");
        String gifId = (String) data.getJSONObject(new Random().nextInt(data.length())).get("id");

        String cachePath = cacheFolder + query + "/" + gifId + ".gif";
        new File(cachePath).getParentFile().mkdirs();
        Files.write(Paths.get(cachePath), requestSender.download(gifId));

        return gifId;
    }

    private void addRecordToHistory(String id, String query, String gifName) throws IOException {
        String gifPath = Paths.get(usersFolder + id + "/" + query + "/" + gifName).toUri().toString();
        String record = String.format("%s,%s,%s\n", LocalDate.now(), query, gifPath);

        String historyPath = usersFolder + id + "/history.csv";
        StandardOpenOption option = Files.exists(Paths.get(historyPath))
                ? StandardOpenOption.APPEND
                : StandardOpenOption.CREATE_NEW;
        Files.write(Paths.get(usersFolder + id + "/history.csv"), record.getBytes(), option);
    }

    private void checkName(String id) {
        if (!Pattern.matches("^\\w+$", id)) {
            throw new IllegalArgumentException();
        }
    }

    public String generateGif(String id, String query, Boolean force) throws IOException {
        checkName(id);
        String gifName, gifPath;

        if ((force == null || !force) && Files.exists(Paths.get(cacheFolder + query))) {
            gifName = copyGifFromCacheToUser(id, query);
        } else {
            gifName = getGifFromServer(query);
            gifName += ".gif";

            String userPath = usersFolder + id + "/" + query + "/" + gifName;
            String cachePath = cacheFolder + query + "/" + gifName;

            new File(userPath).getParentFile().mkdirs();
            Files.copy(Paths.get(cachePath), Paths.get(userPath), StandardCopyOption.REPLACE_EXISTING);
        }
        addRecordToHistory(id, query, gifName);

        gifPath = Paths.get(usersFolder + id + "/" + query + "/" + gifName).toUri().toString();
        addGifToCache(id, query, gifPath);

        return gifPath;
    }

    private void addGifToCache(String id, String query, String gifPath) {
        if (!cache.containsKey(id)) {
            Multimap<String, String> gif = ArrayListMultimap.create();
            gif.put(query, gifPath);
            cache.put(id, gif);
        } else {
            Multimap<String, String> user = cache.get(id);
            if (!user.containsKey(query) || !user.get(query).contains(gifPath)) {
                user.put(query, gifPath);
            }
        }
    }

    public String searchGif(String id, String query, Boolean force) throws IOException {
        checkName(id);

        if ((force == null || !force) && cache.containsKey(id) && cache.get(id).containsKey(query)) {
            List<String> userGifs = new ArrayList<>(cache.get(id).get(query));
            return userGifs.get(new Random().nextInt(userGifs.size()));
        } else {
            Path path = Paths.get(usersFolder + id + "/" + query);
            if (!Files.exists(path)) {
                throw new IOException();
            }
            List<Path> userGifs = Files.list(path).collect(Collectors.toList());
            String gif = userGifs.get(new Random().nextInt(userGifs.size())).toUri().toString();

            addGifToCache(id, query, gif);
            return gif;
        }
    }

    public void deleteUserInformation(String id) throws IOException {
        checkName(id);
        cache.remove(id);
        FileUtils.deleteDirectory(new File(usersFolder + id));
    }

    public void deleteUserCacheByQuery(String id, String query) {
        checkName(id);
        if (query != null && cache.containsKey(id)) {
            cache.get(id).removeAll(query);
        } else {
            cache.remove(id);
        }
    }

    public String getUserHistory(String id) throws IOException {
        checkName(id);
        if (!Files.exists(Paths.get(usersFolder + id))) {
            return "";
        }
        List<String> historyList = Files.readAllLines(Paths.get(usersFolder + id + "/history.csv"));
        JSONArray history = new JSONArray();
        historyList.stream().filter(i -> i.length() > 0).forEach(line -> {
            JSONObject object = new JSONObject();
            String[] lineInfo = line.split(",");
            object.put("date", lineInfo[0]);
            object.put("query", lineInfo[1]);
            object.put("gif", lineInfo[2]);
            history.put(object);
        });

        return history.toString();
    }

    public void deleteUserHistory(String id) throws FileNotFoundException {
        checkName(id);
        if (Files.exists(Paths.get(usersFolder + id))) {
            PrintWriter writer = new PrintWriter(new File(usersFolder + id + "/history.csv"));
            writer.println("");
            writer.close();
        }
    }

    public void deleteCache() {
        cache.clear();
    }

    public String getAllGifs() throws IOException {
        JSONArray gifs = new JSONArray(Files.walk(Paths.get(cacheFolder))
                .filter(Files::isRegularFile)
                .map(Path::toUri)
                .collect(Collectors.toList()));
        return gifs.toString();
    }

    public String downloadGifAndGetSimilar(String query) throws IOException {
        getGifFromServer(query);
        JSONObject response = new JSONObject();
        response.put("query", query);
        List<URI> gifs = Files.list(Paths.get(cacheFolder + query))
                .map(Path::toUri)
                .collect(Collectors.toList());
        response.put("gifs", new JSONArray(gifs));
        return response.toString();
    }

    private JSONArray createJsonFromFolders(List<Path> folders) {
        JSONArray result = new JSONArray();

        folders.forEach(folder -> {

            JSONObject queryGifs = new JSONObject();
            queryGifs.put("query", folder.getFileName());

            try {
                List<URI> gifsInFolder = Files.list(folder)
                        .map(Path::toUri)
                        .collect(Collectors.toList());
                queryGifs.put("gifs", gifsInFolder);
            } catch (IOException ignored) {
            }

            result.put(queryGifs);
        });

        return result;
    }

    public String getUserGifs(String id) throws IOException {
        checkName(id);
        if (!Files.exists(Paths.get(usersFolder + id))) {
            return "";
        }

        List<Path> userQueries = Files.walk(Paths.get(usersFolder + id))
                .filter(Files::isDirectory)
                .filter(folder -> !folder.getFileName().toString().equals(id))
                .collect(Collectors.toList());

        return createJsonFromFolders(userQueries).toString();
    }

    public String getCacheByQuery(String query) throws IOException {
        if (!Files.exists(Paths.get(cacheFolder))) {
            return "";
        }

        JSONArray response = new JSONArray();
        if (query != null) {
            JSONObject queryGifs = new JSONObject();
            queryGifs.put("query", query);
            List<URI> gifs = Files.list(Paths.get(cacheFolder + query)).
                    map(Path::toUri)
                    .collect(Collectors.toList());
            queryGifs.put("gifs", new JSONArray(gifs));
            response.put(queryGifs);
        } else {
            List<Path> cacheQueries = Files.walk(Paths.get(cacheFolder))
                    .filter(Files::isDirectory)
                    .filter(folder -> !folder.getFileName().toString().equals("cache"))
                    .collect(Collectors.toList());

            response = createJsonFromFolders(cacheQueries);
        }
        return response.toString();
    }
}
