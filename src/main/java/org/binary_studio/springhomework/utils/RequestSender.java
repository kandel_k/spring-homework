package org.binary_studio.springhomework.utils;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@PropertySource({ "classpath:dev.properties" })
public class RequestSender {

    @Value("${giphy.api_key}")
    private String key;

    public JSONObject send(String query) {
        String url = "https://api.giphy.com/v1/gifs/search";

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("q", query)
                .queryParam("api_key", key);

        HttpEntity<String> response = new RestTemplate().exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        return new JSONObject(response.getBody());
    }

    public byte[] download(String id) {
        String url = "https://i.giphy.com/media/" + id + "/giphy.gif";
        return new RestTemplate().getForObject(url, byte[].class);
    }
}
