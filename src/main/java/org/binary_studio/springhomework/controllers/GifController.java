package org.binary_studio.springhomework.controllers;

import org.binary_studio.springhomework.services.GifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
public class GifController {

    private final GifService gifService;

    @Autowired
    public GifController(GifService gifService) {
        this.gifService = gifService;
    }

    @PostMapping("/user/{id}/generate")
    public ResponseEntity<?> generateGif(@PathVariable String id,
                                         @RequestParam(name = "query") String query,
                                         @RequestParam(name = "force", required = false) Boolean force) {
        try {
            return ResponseEntity.ok(gifService.generateGif(id, query, force));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/user/{id}/search")
    public ResponseEntity<?> searchGif(@RequestParam(name = "query") String query,
                                       @PathVariable String id,
                                       @RequestParam(name = "force", required = false) Boolean force) {
        try {
            return ResponseEntity.ok(gifService.searchGif(id, query, force));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/user/{id}/clean")
    public ResponseEntity<?> deleteUserInformation(@PathVariable String id) {
        try {
            gifService.deleteUserInformation(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}/reset")
    public ResponseEntity<?> deleteUserCache(@PathVariable String id,
                                             @RequestParam(name = "query", required = false) String query) {
        try {
            gifService.deleteUserCacheByQuery(id, query);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}/history", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getUserHistory(@PathVariable String id) {
        try {
            return ResponseEntity.ok(gifService.getUserHistory(id));
        } catch (IllegalArgumentException | IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/user/{id}/history/clean")
    public ResponseEntity<?> deleteUserHistory(@PathVariable String id) {
        try {
            gifService.deleteUserHistory(id);
        } catch (FileNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/cache")
    public ResponseEntity<?> deleteCache() {
        gifService.deleteCache();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/gifs", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllGifs() {
        try {
            return ResponseEntity.ok(gifService.getAllGifs());
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/cache/generate", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> downloadGif(@RequestParam(name = "query") String query) {
        try {
            return ResponseEntity.ok(gifService.downloadGifAndGetSimilar(query));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/user/{id}/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getUserGifs(@PathVariable String id) {
        try {
            return ResponseEntity.ok(gifService.getUserGifs(id));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/cache", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getCacheByQuery(@RequestParam(name = "query", required = false) String query) {
        try {
            return ResponseEntity.ok(gifService.getCacheByQuery(query));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
