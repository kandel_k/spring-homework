package org.binary_studio.springhomework.system.filters;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@PropertySource("classpath:dev.properties")
@Component
@Order(1)
public class RequestFilter extends OncePerRequestFilter {

    @Value("${giphy.token}")
    String header;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        Logger log = Logger.getLogger(RequestFilter.class.getName());

        log.info("Request: method - " + httpServletRequest.getMethod() + ", url - " + httpServletRequest.getRequestURL());

        if (httpServletRequest.getHeader("X-BSA-GIPHY") == null
                || !httpServletRequest.getHeader("X-BSA-GIPHY").equals(header)) {

            httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
